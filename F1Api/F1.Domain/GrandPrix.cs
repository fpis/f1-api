﻿namespace F1.Domain
{
    public class GrandPrix
    {
        public int GrandPrixId { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string AdditionalInformation { get; set; }
        public int FirstGrandPrix { get; set; }
        public int NumberOfLaps { get; set; }
        public double CircuitLength { get; set; }
        public double RaceDistance { get; set; }
        public TimeSpan LapRecord { get; set; }
        public string LapRecordDriver { get; set; }
        public ICollection<Race> Races { get; set; }
        public ICollection<Driver> Drivers { get; set; }
    }
}