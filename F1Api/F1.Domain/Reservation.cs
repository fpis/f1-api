﻿namespace F1.Domain
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public string PromoCode { get; set; }
        public bool PromoCodeUsed { get; set; }
        public string Token { get; set; }
        public decimal DaysDiscount { get; set; }
        public decimal PromoCodeDiscount { get; set; }
        public decimal EarlyBirdDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime ReservationDate { get; set; }
        public decimal DiscountedPrice { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public IList<ReservationItem> ReservationItems { get; set; }
    }
}