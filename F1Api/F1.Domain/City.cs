﻿using F1Api.Domain;

namespace F1.Domain
{
    public class City{
        public int ZipCode { get; set; }
        public string CityName { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
        public ICollection<Customer> Customers { get; set; }
    }
}