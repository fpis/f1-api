﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Domain
{
    public class Country
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public ICollection<City> Cities { get; set; }
        public ICollection<Driver> Drivers { get; set; }
    }
}
