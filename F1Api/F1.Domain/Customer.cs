﻿using Microsoft.AspNetCore.Identity;

namespace F1.Domain
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? CompanyName { get; set; }
        public string Address1 { get; set; }
        public string? Address2 { get; set; }
        public string Email { get; set; }
        public City City{ get; set; }
        public int CityId { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
    }
}