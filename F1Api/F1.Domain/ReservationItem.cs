﻿using F1Api.Domain;

namespace F1.Domain
{
    public class ReservationItem
    {
        public int SerialNumber { get; set; }
        public int Quantity { get; set; }
        public int DayInItem { get; set; }
        public Reservation Reservation { get; set; }
        public int ReservationId { get; set; }
        public Ticket Ticket { get; set; }
        public int ZoneId { get; set; }
        public int RaceId { get; set; }
    }
}