﻿using F1Api.Domain;
using System.Text.Json.Serialization;

namespace F1.Domain
{
    public class Zone
    {
        public int ZoneId { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public bool SuitableForDisabled { get; set; }
        public bool HasLargeTv { get; set; }
        [JsonIgnore]
        public ICollection<Ticket> Tickets { get; set; }
        [JsonIgnore]
        public ICollection<Race> Races { get; set; }
    }
}