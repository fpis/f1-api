﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Domain
{
    public class Ticket
    {
        public decimal Price { get; set; }
        public int CurrentCapacity { get; set; }
        public Race Race { get; set; }
        public int RaceId { get; set; }
        public Zone Zone { get; set; }
        public int ZoneId { get; set; }
        public ICollection<ReservationItem> ReservationItems { get; set; }
    }
}
