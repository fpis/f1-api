﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Domain
{
    public class Team
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string ColorHex { get; set; }
        public ICollection<Driver> Drivers{ get; set; }
    }
}
