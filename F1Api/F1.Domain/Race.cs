﻿using F1Api.Domain;
using System.Text.Json.Serialization;

namespace F1.Domain
{
    public class Race
    {
        public int RaceId { get; set; }
        public DateTime StartDate { get; set; }
        public string RaceName { get; set; }
        public int GrandPrixId { get; set; }
        public GrandPrix GrandPrix { get; set; }
        [JsonIgnore]
        public ICollection<Ticket> Tickets{ get; set; }
        [JsonIgnore]
        public ICollection<Zone> Zones { get; set; }
    }
}