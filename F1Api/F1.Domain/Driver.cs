﻿using F1Api.Domain;

namespace F1.Domain
{
    public class Driver 
    {
        public int DriverId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public int TotalPoints { get; set; }
        public int ThisYearPoints { get; set; }
        public int Number { get; set; }
        public int Podiums { get; set; }
        public int WorldChampionships { get; set; }
        public string PictureUrl { get; set; }
        public string PictureHelmetUrl { get; set; }
        public string Biography { get; set; }
        public ICollection<GrandPrix> GrandPrixes{ get; set; }
        public Team Team{ get; set; }
        public int TeamId { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
    }
}