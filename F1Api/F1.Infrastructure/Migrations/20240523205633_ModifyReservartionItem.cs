﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class ModifyReservartionItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TicketType",
                table: "ReservationItems",
                newName: "DayInItem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DayInItem",
                table: "ReservationItems",
                newName: "TicketType");
        }
    }
}
