﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class AddingCapacityConstraint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddCheckConstraint(
                name: "CK_Current_Capacity_Non_Negative",
                table: "Ticket",
                sql: "CurrentCapacity>=0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropCheckConstraint(
                name: "CK_Current_Capacity_Non_Negative",
                table: "Ticket");
        }
    }
}
