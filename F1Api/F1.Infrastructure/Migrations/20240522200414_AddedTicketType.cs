﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class AddedTicketType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TicketType",
                table: "ReservationItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReservationItems",
                table: "ReservationItems");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReservationItems",
                table: "ReservationItems",
                columns: new[] { "ReservationId", "SerialNumber", "TicketType" });

            migrationBuilder.AddForeignKey(
                name: "FK_Drivers_Countries_CountryId",
                table: "Drivers",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Drivers_Teams_TeamId",
                table: "Drivers",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "TeamId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
               name: "FK_Drivers_Teams_TeamId",
               table: "Cities");

            migrationBuilder.DropForeignKey(
               name: "FK_Drivers_Countries_CountryId",
               table: "Cities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReservationItems",
                table: "ReservationItems");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReservationItems",
                table: "ReservationItems",
                column: "TicketType");

        }
    }
}
