﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class AddGrandPrixInformation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "CircuitLength",
                table: "GrandPrixes",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "FirstGrandPrix",
                table: "GrandPrixes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "LapRecord",
                table: "GrandPrixes",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "LapRecordDriver",
                table: "GrandPrixes",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "NumberOfLaps",
                table: "GrandPrixes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "RaceDistance",
                table: "GrandPrixes",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CircuitLength",
                table: "GrandPrixes");

            migrationBuilder.DropColumn(
                name: "FirstGrandPrix",
                table: "GrandPrixes");

            migrationBuilder.DropColumn(
                name: "LapRecord",
                table: "GrandPrixes");

            migrationBuilder.DropColumn(
                name: "LapRecordDriver",
                table: "GrandPrixes");

            migrationBuilder.DropColumn(
                name: "NumberOfLaps",
                table: "GrandPrixes");

            migrationBuilder.DropColumn(
                name: "RaceDistance",
                table: "GrandPrixes");
        }
    }
}
