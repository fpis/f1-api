﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class SolveRaceAndZoneId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservationItems_Ticket_ZoneId_RaceId",
                table: "ReservationItems");

            migrationBuilder.DropIndex(
                name: "IX_ReservationItems_ZoneId_RaceId",
                table: "ReservationItems");

            migrationBuilder.CreateIndex(
                name: "IX_ReservationItems_RaceId_ZoneId",
                table: "ReservationItems",
                columns: new[] { "RaceId", "ZoneId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ReservationItems_Ticket_RaceId_ZoneId",
                table: "ReservationItems",
                columns: new[] { "RaceId", "ZoneId" },
                principalTable: "Ticket",
                principalColumns: new[] { "RaceId", "ZoneId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservationItems_Ticket_RaceId_ZoneId",
                table: "ReservationItems");

            migrationBuilder.DropIndex(
                name: "IX_ReservationItems_RaceId_ZoneId",
                table: "ReservationItems");

            migrationBuilder.CreateIndex(
                name: "IX_ReservationItems_ZoneId_RaceId",
                table: "ReservationItems",
                columns: new[] { "ZoneId", "RaceId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ReservationItems_Ticket_ZoneId_RaceId",
                table: "ReservationItems",
                columns: new[] { "ZoneId", "RaceId" },
                principalTable: "Ticket",
                principalColumns: new[] { "RaceId", "ZoneId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
