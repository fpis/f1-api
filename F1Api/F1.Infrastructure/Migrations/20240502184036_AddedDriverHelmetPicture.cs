﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace F1Api.Infrastructure.Migrations
{
    public partial class AddedDriverHelmetPicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorldChampionchips",
                table: "Drivers",
                newName: "WorldChampionships");

            migrationBuilder.AddColumn<string>(
                name: "PictureHelmetUrl",
                table: "Drivers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PictureHelmetUrl",
                table: "Drivers");

            migrationBuilder.RenameColumn(
                name: "WorldChampionships",
                table: "Drivers",
                newName: "WorldChampionchips");
        }
    }
}
