﻿using F1.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Infrastructure
{
    public static class DbHelper
    {
        private static void EnableIdentityInsert<T>(this F1DbContext context)
        {
            var entityType = context.Model.FindEntityType(typeof(T));
            var value = "ON";
            context.Database.ExecuteSqlRaw(
                $"SET IDENTITY_INSERT {entityType.GetSchema()}.{entityType.GetTableName()} {value}");
        }

        private static void DisableIdentityInsert<T>(this F1DbContext context)
        {
            var entityType = context.Model.FindEntityType(typeof(T));
            var value = "OFF";
            context.Database.ExecuteSqlRaw(
                $"SET IDENTITY_INSERT {entityType.GetSchema()}.{entityType.GetTableName()} {value}");
        }

        public static void SaveChangesWithIdentityInsert<T>(this F1DbContext context)
        {
            try
            {
                using var transaction = context.Database.BeginTransaction();
                context.EnableIdentityInsert<T>();
                context.SaveChanges();
                context.DisableIdentityInsert<T>();
                transaction.Commit();
            }catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
