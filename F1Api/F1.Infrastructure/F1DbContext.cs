﻿using F1.Domain;
using F1Api.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;
using System.Reflection.Metadata;

namespace F1.Infrastructure
{
    public class F1DbContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Driver> Drivers{ get; set; }
        public DbSet<GrandPrix> GrandPrixes { get; set; }
        public DbSet<Race> Races{ get; set; }
        public DbSet<Reservation> Reservations{ get; set; }
        public DbSet<ReservationItem> ReservationItems{ get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public F1DbContext(DbContextOptions<F1DbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(m => Console.WriteLine(m))
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Unique promo code
            modelBuilder.Entity<Reservation>()
                .HasIndex(e => e.PromoCode)
                .IsUnique();

            //Unique customer Email
            modelBuilder.Entity<Customer>()
                .HasIndex(e => e.Email)
                .IsUnique();

            //Quantity > 0
            modelBuilder.Entity<Ticket>(
                entity => entity.HasCheckConstraint("CK_Current_Capacity_Non_Negative", "CurrentCapacity>=0"));

            //City 1 - 0..* Customer
            modelBuilder.Entity<City>()
                .HasMany(e => e.Customers)
                .WithOne(e => e.City)
                .HasForeignKey(e => e.CityId)
                .IsRequired();

            //Customer 1 - 0..* Reservation
            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Reservations)
                .WithOne(e => e.Customer)
                .HasForeignKey(e => e.CustomerId)
                .IsRequired();

            //Reservation 1 - 1..* ReservationItem
            modelBuilder.Entity<Reservation>()
                .HasMany(e => e.ReservationItems)
                .WithOne(e => e.Reservation)
                .HasForeignKey(e => e.ReservationId)
                .IsRequired();


            //GrandPrix 1 - 1..* Race
            modelBuilder.Entity<GrandPrix>()
                .HasMany(e => e.Races)
                .WithOne(e => e.GrandPrix)
                .HasForeignKey(e => e.GrandPrixId)
                .IsRequired();

            //GrandPrix 0..* - 1..* Driver
            modelBuilder.Entity<GrandPrix>()
                .HasMany(e => e.Drivers)
                .WithMany(e => e.GrandPrixes);
            
            //Ticket Primary Key
            modelBuilder.Entity<Ticket>()
                .HasKey(ticket => new {ticket.RaceId, ticket.ZoneId});

            //Zone 1..* - 1..* Race --- Ticket
            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Race)
                .WithMany(r => r.Tickets)
                .HasForeignKey(t => t.RaceId);

            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Zone)
                .WithMany(z => z.Tickets)
                .HasForeignKey(t => t.ZoneId);

            //Ticket 1 - 0..* ReservationItem
            modelBuilder.Entity<Ticket>()
                .HasMany(t => t.ReservationItems)
                .WithOne(ri => ri.Ticket)
                .HasForeignKey(t => new {t.RaceId, t.ZoneId})
                .IsRequired();

            //ReservationItem Primary Key
            modelBuilder.Entity<ReservationItem>()
                .HasKey(ri => new {ri.ReservationId, ri.SerialNumber, ri.DayInItem});

            modelBuilder.Entity<ReservationItem>()
                .Property(t => t.SerialNumber)
                .ValueGeneratedNever();

            //City Primary Key
            modelBuilder.Entity<City>()
                .HasKey(c => c.ZipCode);

            //Customer 1 - 0..* Reservation
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Reservations)
                .WithOne(r => r.Customer)
                .HasForeignKey(r => r.CustomerId)
                .IsRequired();

            //Team 1 - 0..* Drivers
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Drivers)
                .WithOne(d => d.Team)
                .HasForeignKey(t => t.TeamId)
                .IsRequired();

            //Country 1 - 0..* Drivers
            modelBuilder.Entity<Country>()
                .HasMany(t => t.Drivers)
                .WithOne(d => d.Country)
                .HasForeignKey(t => t.CountryId)
                .IsRequired();

            //Country 1 - 0..* City
            modelBuilder.Entity<Country>()
                .HasMany(t => t.Cities)
                .WithOne(d => d.Country)
                .HasForeignKey(t => t.CountryId)
                .IsRequired();

            //Team 1 - 0..* Driver
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Drivers)
                .WithOne(d => d.Team)
                .HasForeignKey(t => t.TeamId)
                .IsRequired();

            //Country 1 - 0..* Driver
            modelBuilder.Entity<Country>()
                .HasMany(t => t.Drivers)
                .WithOne(d => d.Country)
                .HasForeignKey(t => t.CountryId)
                .IsRequired();
        }
    }
}