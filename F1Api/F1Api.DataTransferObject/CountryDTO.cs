﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class CountryDTO
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
