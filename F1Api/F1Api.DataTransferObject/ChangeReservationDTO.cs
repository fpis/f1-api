﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ChangeReservationDTO
    {
        public int ReservationId { get; set; }
        public string Token { get; set; }
        public List<ChangeReservationItemsDTO> ReservationItems { get; set; }
    }
}
