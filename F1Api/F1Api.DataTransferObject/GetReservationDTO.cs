﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class GetReservationDTO
    {
        public int ReservationId { get; set; }
        public DateTime ReservationDate { get; set; }
        public decimal DaysDiscount { get; set; }
        public decimal PromoCodeDiscount { get; set; }
        public decimal EarlyBirdDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public string CustomerFullName { get; set; }
        public List<GetReservationItemsDTO> ReservationItems { get; set; }
    }
}
