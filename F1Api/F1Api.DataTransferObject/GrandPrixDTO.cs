﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class GrandPrixDTO
    {
        [Required]
        public int GrandPrixId { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public string AdditionalInformation { get; set; }
        [Required]
        public int FirstGrandPrix { get; set; }
        [Required]
        public int NumberOfLaps { get; set; }
        [Required]
        public double CircuitLength { get; set; }
        [Required]
        public double RaceDistance { get; set; }
        [Required]
        public TimeSpan LapRecord { get; set; }
        [Required]
        public string LapRecordDriver { get; set; }
    }
}
