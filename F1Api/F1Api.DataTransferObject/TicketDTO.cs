﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class TicketDTO
    {
        public List<int> RaceIds { get; set; }
        [JsonIgnore]
        public int RaceId {  get; set; }
        public int ZoneId { get; set; }
        public decimal Price { get; set; }
        public decimal DaysDiscount { get; set; }
        public decimal EarlyBirdDiscount { get; set; }
        public int CurrentCapacity { get; set; }
        public string ZoneName { get; set; }
        public string Days { get; set; }
        public DateTime? RaceDate { get; set; }

    }
}
