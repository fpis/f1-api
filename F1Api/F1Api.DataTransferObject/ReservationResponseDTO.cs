﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ReservationResponseDTO
    {
        public string PromoCode { get; set; }
        public string Token { get; set; }
    }
}
