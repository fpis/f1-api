﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ReservationRequestDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? CompanyName { get; set; }
        public string Address1 { get; set; }
        public string? Address2 { get; set; }
        public int CountryId { get; set; }
        public int PostalCode { get; set; }
        public string CityName { get; set; }
        public string Email { get; set; }
        public string? PromoCode{ get; set; }
        public List<ReservationItemsDTO> ReservationItems { get; set; }
    }
}
