﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ReservationItemsDTO
    {
        public List<int> RaceIds { get; set; }
        [JsonIgnore]
        public int RaceId { get; set; }
        public int ZoneId { get; set; }
        public int Quantity { get; set; }
        [JsonIgnore]
        public decimal Price { get; set; }

    }
}
