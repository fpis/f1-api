﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class DriverBaseInfoDTO
    {
        public int DriverId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TeamName { get; set; }
        public int ThisYearPoints { get; set; }
        public string CountryShortName { get; set; }
        public string PictureUrl { get; set; }
        public int Number { get; set; }
        public string ColorHex { get; set; }
    }
}
