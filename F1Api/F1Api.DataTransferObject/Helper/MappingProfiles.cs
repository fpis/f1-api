﻿using AutoMapper;
using F1.Domain;
using F1Api.Domain;

namespace F1Api.DataTransferObject
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<GrandPrix, GrandPrixDTO>();

            CreateMap<Driver, DriverFullInfoDTO>()
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.TeamName))
                .ForMember(dest => dest.ColorHex, opt => opt.MapFrom(src => src.Team.ColorHex))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country.Name))
                .ForMember(dest => dest.CountryShortName, opt => opt.MapFrom(src => src.Country.ShortName));
            CreateMap<Driver, DriverBaseInfoDTO>()
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.TeamName))
                .ForMember(dest => dest.ColorHex, opt => opt.MapFrom(src => src.Team.ColorHex))
                .ForMember(dest => dest.CountryShortName, opt => opt.MapFrom(src => src.Country.ShortName));

            CreateMap<Ticket, TicketDTO>()
                .ForMember(dest => dest.ZoneName, opt => opt.MapFrom(src => src.Zone.Name))
                .ForMember(dest => dest.RaceDate, opt => opt.MapFrom(src => src.Race.StartDate))
                .ForMember(dest => dest.ZoneId, opt => opt.MapFrom(src => src.Zone.ZoneId))
                .ForMember(dest => dest.RaceId, opt => opt.MapFrom(src => src.Race.RaceId))
                .ForMember(dest => dest.Days, opt => opt.MapFrom(src => src.Race.StartDate.DayOfWeek));

            CreateMap<Reservation, ReservationResponseDTO>();

            CreateMap<ReservationItemsDTO, ReservationItem>()
                .ForMember(dest => dest.RaceId, opt => opt.MapFrom(src => src.RaceIds));

            CreateMap<ReservationRequestDTO, Reservation>()
                .ForMember(dest => dest.PromoCode, opt => opt.MapFrom(src => src.PromoCode))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => new Customer
                {
                    Address1 = src.Address1,
                    Address2 = src.Address2,
                    CompanyName = src.CompanyName,
                    Email = src.Email,
                    FirstName = src.FirstName,
                    LastName = src.LastName,
                    City = new City
                    {
                        ZipCode = src.PostalCode,
                        CityName = src.CityName,
                        CountryId = src.CountryId
                    }
                }))
                .ForMember(dest => dest.ReservationItems, opt => opt.MapFrom(src => src.ReservationItems.SelectMany((item) => item.RaceIds.Select(item2 => new ReservationItem
                {
                    Quantity = item.Quantity,
                    SerialNumber = src.ReservationItems.IndexOf(item) + 1,
                    ZoneId = item.ZoneId,
                    DayInItem = item.RaceIds.IndexOf(item2) + 1,
                    RaceId = item2,
                }).ToList()
                )));

            CreateMap<Reservation, GetReservationDTO>()
                .ForMember(dest => dest.CustomerFullName, opt => opt.MapFrom(src => $"{src.Customer.FirstName} {src.Customer.LastName}"))
                .ForMember(dest => dest.ReservationItems, opt => opt.MapFrom(src => new List<GetReservationItemsDTO>()));  

            CreateMap<Country, CountryDTO>();
            CreateMap<ChangeReservationDTO, Reservation>()
                .ForMember(dest => dest.ReservationItems, opt => opt.MapFrom(src => src.ReservationItems));
        }
    }
}