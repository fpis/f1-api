﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject.Helper
{
    public  class TicketFilters
    {
        public bool HasLargeTv { get; set; }
        public bool SuitableForDisabled { get; set; }
        public decimal LowPrice { get; set; }
        public decimal HighPrice { get; set; }
        public List<int> Days { get; set; }
    }
}
