﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ReservationDTO
    {
        public decimal DaysDiscount { get; set; }
        public decimal PromoCodeDiscount { get; set; }
        public decimal EarlyBirdDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal DiscountedPrice { get; set; }
        public ICollection<ReservationItemsDTO> ReservationItems { get; set; }
    }
}
