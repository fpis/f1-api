﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class GetReservationItemsDTO
    {
        public int Quantity { get; set; }
        public int SerialNumber { get; set; }
        public int MaximumQuantity { get; set; }
        public string Days { get; set; }
        public decimal Price { get; set; }
        public decimal FullPrice {  get; set; }
        public string ZoneName { get; set; }
        [JsonIgnore]
        public int NumberOfDays { get; set; }
    }
}
