﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class DriverFullInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public int TotalPoints { get; set; }
        public int Number { get; set; }
        public string Country { get; set; }
        public int Podiums { get; set; }
        public int WorldChampionships { get; set; }
        public string PictureHelmetUrl { get; set; }
        public string PictureUrl { get; set; }
        public string Biography { get; set; }
        public string TeamName { get; set; }
        public string ColorHex { get; set; }
        public string CountryShortName { get; set; }
    }
}
