﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataTransferObject
{
    public class ChangeReservationItemsDTO
    {
        public int SerialNumber { get; set; }
        public int Quantity { get; set; }
    }
}
