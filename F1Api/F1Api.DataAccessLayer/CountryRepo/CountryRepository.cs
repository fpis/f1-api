﻿using F1.Infrastructure;
using F1Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.CountryRepo
{
    public class CountryRepository : ICountryRepository
    {
        private readonly F1DbContext _dbContext;

        public CountryRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public ICollection<Country> GetAll()
        {
            return _dbContext.Countries.ToList();
        }
    }
}
