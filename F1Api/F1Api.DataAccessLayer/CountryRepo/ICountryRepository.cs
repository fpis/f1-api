﻿using F1Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.CountryRepo
{
    public interface ICountryRepository
    {
        public ICollection<Country> GetAll();
    }
}
