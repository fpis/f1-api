﻿using F1.Domain;
using F1.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.CityRepo
{
    public class CityRepository : ICityRepository
    {
        private readonly F1DbContext _dbContext;

        public CityRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public City? GetById(int zipCode)
        {
            return _dbContext.Cities.FirstOrDefault(city => city.ZipCode == zipCode);
        }
    }
}
