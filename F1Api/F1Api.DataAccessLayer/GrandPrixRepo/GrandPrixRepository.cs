﻿using F1.Domain;
using F1.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.GrandPrixRepo
{
    internal class GrandPrixRepository : IGrandPrixRepository
    {
        private readonly F1DbContext _dbContext;

        public GrandPrixRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public GrandPrix? GetByName(string name)
        {
            return _dbContext.GrandPrixes.FirstOrDefault(gp => gp.City == name);
        }
    }
}
