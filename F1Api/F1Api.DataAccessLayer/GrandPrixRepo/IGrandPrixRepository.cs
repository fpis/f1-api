﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.GrandPrixRepo
{
    public interface IGrandPrixRepository
    {
        public GrandPrix? GetByName(string name);
    }
}
