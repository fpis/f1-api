﻿using BCrypt.Net;
using F1.Domain;
using F1.Infrastructure;
using F1Api.DataTransferObject;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.ReservationRepo
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly F1DbContext _dbContext;

        public ReservationRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void DeactivatePromoCode(string promoCode)
        {
            List<Reservation> reservations = _dbContext.Reservations.ToList();
            Reservation r = reservations.Where(r => BCrypt.Net.BCrypt.EnhancedVerify(promoCode, r.PromoCode)).FirstOrDefault();
            if(r != null)
            {
                r.PromoCodeUsed = true;
                _dbContext.Update(r);
            }
        }

        public bool Delete(int id)
        {
            var entityToDelete = _dbContext.Reservations
                .Include(r => r.ReservationItems)
                .ThenInclude(ri => ri.Ticket)
                .FirstOrDefault(r => r.ReservationId == id);
            foreach(var reservationItem in entityToDelete.ReservationItems)
            {
                reservationItem.Ticket.CurrentCapacity += reservationItem.Quantity;
                _dbContext.Update(reservationItem.Ticket);
            }
            entityToDelete.ReservationItems = null;
            if(entityToDelete != null)
            {
                _dbContext.Remove(entityToDelete);
                return true;
            }
            return false;
        }

        public string? GetPromoCode(string promoCode)
        {
            Reservation r = _dbContext.Reservations.FirstOrDefault(r => r.PromoCode.ToLower() == promoCode.ToLower());
            if (r == null)
            {
                return null;
            }
            if (r.PromoCodeUsed)
            {
                return string.Empty;
            }
            return r.PromoCode;
        }

        public Reservation GetReservationByEmailAndToken(string email, string token)
        {
            List<Reservation> reservations = _dbContext.Reservations
                .Include(r => r.Customer)
                .Include(r => r.ReservationItems)
                    .ThenInclude(ri => ri.Ticket)
                        .ThenInclude(t => t.Zone)
                .Where(r => r.Customer.Email == email)
                .ToList();

            foreach (var reservation in reservations)
            {
                reservation.ReservationItems = reservation.ReservationItems.OrderBy(ri => ri.SerialNumber).ThenByDescending(ri => ri.DayInItem).ToList();
            }
            Reservation r = reservations.Where(r => BCrypt.Net.BCrypt.EnhancedVerify(token, r.Token)).FirstOrDefault();
            return r;
        }

        public string? GetToken(string token)
        {
            Reservation r = _dbContext.Reservations.FirstOrDefault(r => r.Token.ToLower() == token.ToLower());
            if (r == null)
            {
                return null;
            }
            return r.Token;
        }

        public Reservation SaveReservation(Reservation reservation)
        {
            var customer = _dbContext.Customers.FirstOrDefault(c => c.Email == reservation.Customer.Email);
            if(customer != null)
            {
                reservation.Customer = null;
                reservation.CustomerId = customer.CustomerId;
            }
            return _dbContext.Reservations.Add(reservation).Entity;
        }

        public bool UpdateReservationItems(ChangeReservationDTO reservation)
        {
            Reservation reser = _dbContext.Reservations.Include(r => r.ReservationItems).ThenInclude(ri => ri.Ticket).FirstOrDefault(r => r.ReservationId == reservation.ReservationId);
            if(reser == null)
            {
                return false;
            }
            if(!BCrypt.Net.BCrypt.EnhancedVerify(reservation.Token, reser.Token))
            {
                return false;
            }

            List<ReservationItem> itemsToUpdate = new List<ReservationItem>();
            decimal oldTotalPrice = reser.TotalPrice;
            decimal daysDiscountPercent = reser.DaysDiscount / oldTotalPrice;
            decimal promoCodeDiscountPercent = reser.PromoCodeDiscount / oldTotalPrice;
            decimal earlyBirdDiscountPercent = reser.EarlyBirdDiscount / oldTotalPrice;
            decimal newTotalPrice = 0;
            foreach (var item in reservation.ReservationItems)
            {
                foreach(var resItem in reser.ReservationItems)
                {
                    if(item.SerialNumber == resItem.SerialNumber)
                    {
                        resItem.Ticket.CurrentCapacity += resItem.Quantity - item.Quantity;
                        resItem.Quantity = item.Quantity;
                        newTotalPrice += resItem.Quantity * resItem.Ticket.Price;
                        itemsToUpdate.Add(resItem);
                    }
                }
            }
            Reservation reservationToUpdate = reser;
            reservationToUpdate.ReservationItems = itemsToUpdate;
            reservationToUpdate.DaysDiscount = newTotalPrice * daysDiscountPercent;
            reservationToUpdate.EarlyBirdDiscount = newTotalPrice * earlyBirdDiscountPercent;
            reservationToUpdate.PromoCodeDiscount = newTotalPrice * promoCodeDiscountPercent;
            reservationToUpdate.TotalPrice = newTotalPrice;
            reservationToUpdate.DiscountedPrice = newTotalPrice - reservationToUpdate.DaysDiscount - reservationToUpdate.EarlyBirdDiscount - reservationToUpdate.PromoCodeDiscount;
            try
            {
                _dbContext.Update(reservationToUpdate);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

        }

        public string? VerifyPromoCode(string promoCode)
        {
            List<Reservation> reservations = _dbContext.Reservations.ToList();
            Reservation r = reservations.Where(r => BCrypt.Net.BCrypt.EnhancedVerify(promoCode, r.PromoCode)).FirstOrDefault();
            if (r == null || r.PromoCodeUsed)
            {
                return null;
            }
            return r.PromoCode;
        }
    }
}
