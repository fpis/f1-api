﻿using F1.Domain;
using F1Api.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.ReservationRepo
{
    public interface IReservationRepository
    {
        public Reservation SaveReservation(Reservation reservation);
        public string? GetPromoCode(string promoCode);
        public string? GetToken(string promoCode);
        public void DeactivatePromoCode(string promoCode);
        public string? VerifyPromoCode(string promoCode);
        public Reservation GetReservationByEmailAndToken(string email, string token);
        public bool Delete(int id);
        public bool UpdateReservationItems(ChangeReservationDTO reservation);
    }
}
