﻿using F1.Domain;
using F1.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.DriverRepo
{
    internal class DriverRepository : IDriverRepository
    {
        private readonly F1DbContext _dbContext;

        public DriverRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Driver? GetById(int id)
        {
            return _dbContext.Drivers
                .Include(driver => driver.Team)
                .Include(driver => driver.Country)
                .FirstOrDefault(d => d.DriverId == id);
        }

        public IQueryable<Driver> GetDriversByGrandPrix(int page, int size, int grandPrixId)
        {
            return _dbContext.Drivers
                .Include(driver => driver.Team)
                .Include(driver => driver.Country)
                .Where(d => d.GrandPrixes.Any(gp => gp.GrandPrixId ==  grandPrixId))
                .Skip((page - 1) * size)
                .Take(size);
        }
    }
}
