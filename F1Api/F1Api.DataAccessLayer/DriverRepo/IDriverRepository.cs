﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.DriverRepo
{
    public interface IDriverRepository
    {
        public Driver? GetById(int id);
        public IQueryable<Driver> GetDriversByGrandPrix(int page, int size, int grandPrixId);
    }
}
