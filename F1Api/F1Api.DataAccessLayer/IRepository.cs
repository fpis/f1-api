﻿namespace F1Api.DataAccessLayer
{
    public interface IRepository<T> where T : class
    {
        public IEnumerable<T> GetAll();
        public void Add(T entity);
        public T? GetById(int id);
    }
}