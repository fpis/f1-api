﻿using F1.Infrastructure;
using F1Api.Domain;
using F1Api.DataTransferObject.Helper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using F1Api.DataTransferObject;

namespace F1Api.DataAccessLayer.TicketRepo
{
    public class TicketRepository : ITicketRepository
    {
        private readonly F1DbContext _dbContext;

        public TicketRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int GetMaximumQuantity(int raceId, int zoneId)
        {
            return _dbContext.Ticket
                .FirstOrDefault(t => t.RaceId == raceId && t.ZoneId == zoneId).CurrentCapacity;
        }

        public ICollection<Ticket> GetTicketByFilters(TicketFilters filters)
        {
            return _dbContext.Ticket
                .Include(ticket => ticket.Zone)
                .Include(ticket => ticket.Race)
                .Where(t => t.Zone.HasLargeTv == filters.HasLargeTv && t.Zone.SuitableForDisabled == filters.SuitableForDisabled && t.CurrentCapacity > 0)
                .OrderBy(t => t.Race.StartDate)
                .ToList();
        }

        public ICollection<Ticket> GetTicketsById(List<ReservationItemsDTO> reservationItems)
        {
            return reservationItems
                .Join(_dbContext.Ticket,
                    reservationItem => new { RaceId = reservationItem.RaceId, ZoneId = reservationItem.ZoneId },
                    ticket => new { RaceId = ticket.RaceId, ZoneId = ticket.ZoneId },
                    (reservationItem, ticket) => ticket)
                .Distinct()
                 .ToList();
        }
    }
}
