﻿using F1.Domain;
using F1Api.DataTransferObject;
using F1Api.DataTransferObject.Helper;
using F1Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.TicketRepo
{
    public interface ITicketRepository
    {
        public ICollection<Ticket> GetTicketByFilters(TicketFilters filters);
        public ICollection<Ticket> GetTicketsById(List<ReservationItemsDTO> reservationItems);
        public int GetMaximumQuantity(int raceId, int zoneId);
    }
}
