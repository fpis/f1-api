﻿using F1.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.RaceRepo
{
    public class RaceRepository : IRaceRepository
    {
        private readonly F1DbContext _dbContext;

        public RaceRepository(F1DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public string GetDayInWeekNameById(int raceId)
        {
            return _dbContext.Races
                .FirstOrDefault(r => r.RaceId == raceId)
                .StartDate.ToString("dddd");
        }
    }
}
