﻿using F1Api.DataAccessLayer.CityRepo;
using F1Api.DataAccessLayer.CountryRepo;
using F1Api.DataAccessLayer.DriverRepo;
using F1Api.DataAccessLayer.GrandPrixRepo;
using F1Api.DataAccessLayer.RaceRepo;
using F1Api.DataAccessLayer.ReservationRepo;
using F1Api.DataAccessLayer.TicketRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.UOW
{
    public interface IUnitOfWork
    {
        public IGrandPrixRepository GrandPrixRepository { get; set; }
        public IDriverRepository DriverRepository { get; set; }
        public ITicketRepository TicketRepository { get; set; }
        public IReservationRepository ReservationRepository { get; set; }
        public ICityRepository CityRepository { get; set; }
        public ICountryRepository CountryRepository { get; set; }
        public IRaceRepository RaceRepository { get; set; }
        public void SaveChanges();
        public void SaveChangesWithIdentityInsertOn<T>();
    }
}
