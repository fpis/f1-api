﻿using F1.Infrastructure;
using F1Api.DataAccessLayer.CityRepo;
using F1Api.DataAccessLayer.CountryRepo;
using F1Api.DataAccessLayer.DriverRepo;
using F1Api.DataAccessLayer.GrandPrixRepo;
using F1Api.DataAccessLayer.RaceRepo;
using F1Api.DataAccessLayer.ReservationRepo;
using F1Api.DataAccessLayer.TicketRepo;
using F1Api.Infrastructure;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.DataAccessLayer.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly F1DbContext _context;
        public IGrandPrixRepository GrandPrixRepository{ get; set; }
        public IDriverRepository DriverRepository{ get; set; }
        public ITicketRepository TicketRepository { get; set; }
        public IReservationRepository ReservationRepository { get; set; }
        public ICountryRepository CountryRepository { get; set; }
        public ICityRepository CityRepository { get; set; }
        public IRaceRepository RaceRepository { get; set; }
        public UnitOfWork(F1DbContext dbContext)
        {
            _context = dbContext;
            GrandPrixRepository = new GrandPrixRepository(dbContext);
            DriverRepository = new DriverRepository(dbContext);
            TicketRepository = new TicketRepository(dbContext);
            ReservationRepository = new ReservationRepository(dbContext);
            CityRepository = new CityRepository(dbContext);
            CountryRepository = new CountryRepository(dbContext);
            RaceRepository = new RaceRepository(dbContext);
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void SaveChangesWithIdentityInsertOn<T>()
        {
            try
            {
                _context.SaveChangesWithIdentityInsert<T>();
            }catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
