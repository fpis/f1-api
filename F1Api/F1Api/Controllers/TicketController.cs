﻿using AutoMapper;
using F1Api.DataTransferObject.Helper;
using F1Api.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace F1Api.Controllers
{
    [Route("api/ticket")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketService _ticketService;
        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost("get-tickets")]
        public IActionResult GetTickets([FromBody] TicketFilters filters)
        {
            if (filters == null)
            {
                return BadRequest("Filters missing");
            }
            if(filters.LowPrice > filters.HighPrice)
            {
                return BadRequest("Lower price have to be lower than higher price");
            }
            if(filters.LowPrice == 0 && filters.HighPrice == 0)
            {
                filters.LowPrice = int.MinValue;
                filters.HighPrice = int.MaxValue;
            }
            if(filters.Days.Any(n => n != 1 && n != 2 && n != 3))
            {
                return BadRequest("Days are only 1, 2 or 3");
            }
            if(filters.Days.GroupBy(x => x).Any(d => d.Count()>1))
            {
                return BadRequest("Days cannot have duplicate values");
            }
            filters.Days.Sort();
            return Ok(_ticketService.GetTicketByFilters(filters));
        }
    }
}
