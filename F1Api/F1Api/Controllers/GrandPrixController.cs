﻿using AutoMapper;
using F1.Domain;
using F1Api.DataTransferObject;
using F1Api.Services.Implementation;
using F1Api.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace F1Api.Controllers
{
    [Route("api/grand-prix")]
    [ApiController]
    public class GrandPrixController : ControllerBase
    {
        private readonly IGrandPrixService _grandPrixService;
        private readonly IMapper _mapper;

        public GrandPrixController(IGrandPrixService grandPrixService, IMapper mapper)
        {
            _grandPrixService = grandPrixService;
            _mapper = mapper;
        }

        [HttpGet("get-by-name")]
        public IActionResult GetByName(string name)
        {
            GrandPrix? grandPrix = _grandPrixService.GetByName(name);
            return grandPrix == null ? NotFound("There is no Grand Prix with provided name") : Ok(_mapper.Map<GrandPrixDTO>(grandPrix));
        }
    }
}
