﻿using AutoMapper;
using F1.Domain;
using F1Api.DataTransferObject;
using F1Api.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace F1Api.Controllers
{
    [Route("api/driver")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly IDriverService _driverService;
        private readonly IMapper _mapper;

        public DriverController(IMapper mapper, IDriverService driverService)
        {
            _mapper = mapper;
            _driverService = driverService;
        }
        [HttpGet("get-driver")]
        public IActionResult GetById(int driverId)
        {
            if (driverId < 1)
            {
                return BadRequest("Invalid Driver Id (>= 1)");
            }
            Driver? driver = _driverService.GetById(driverId);
            return driver != null? Ok(_mapper.Map<DriverFullInfoDTO>(driver)) : NotFound("There is no driver with provided id");
        }

        [HttpGet("get-drivers")]
        public IActionResult GetDrivers([FromQuery]int page, [FromQuery]int size, [FromQuery]int grandPrixId)
        {
            if(page < 1 || size < 1)
            {
                return BadRequest("Page or size must be 1 or higher");
            }
            if(grandPrixId < 1)
            {
                return BadRequest("Invalid Grand Prix Id (>= 1)");
            }
            ICollection<DriverBaseInfoDTO> driversDTO = _driverService
                .GetDriversByGrandPrix(page, size, grandPrixId)
                .Select(driver => _mapper.Map<DriverBaseInfoDTO>(driver))
                .ToList();
            return Ok(driversDTO);
        }
    }
}
