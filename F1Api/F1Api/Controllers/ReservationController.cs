﻿using AutoMapper;
using F1.Domain;
using F1Api.DataAccessLayer.ReservationRepo;
using F1Api.DataTransferObject;
using F1Api.Services.Exceptions.Reservation;
using F1Api.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace F1Api.Controllers
{
    [Route("api/reservation")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly IReservationService _reservationService;

        public ReservationController(IReservationService reservationService)
        {
            _reservationService = reservationService;
        }

        [HttpPost("make-reservation")]
        public IActionResult MakeReservation(ReservationRequestDTO requestDTO)
        {
            try
            {
                return Ok(_reservationService.AddReservation(requestDTO));
            }
            catch(NonNegativeCapacityException ex) 
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("verify-promo-code")]
        public IActionResult VerifyPromoCode(string promoCode)
        {
            if(_reservationService.VerifyPromoCode(promoCode) == null)
            {
                return NotFound($@"{promoCode} isn't active or does not exist");
            }
            return Ok($"{promoCode} is okay");
        }

        [HttpGet("get-reservation")]
        public IActionResult GetReservation(string token, string email)
        {
            if (token.Length <= 1 && email.Length <= 1)
            {
                return BadRequest("Provided token or email is not in correct format");
            }
            GetReservationDTO returnReservation = _reservationService.GetReservation(token, email);
            if (returnReservation == null)
            {
                return NotFound("Cannot found reservation with this parameters");
            }
            return Ok(returnReservation);
        }

        [HttpDelete("delete-reservation")]
        public IActionResult DeleteReservation(int reservationId)
        {
            if (_reservationService.Delete(reservationId))
            {
                return Ok("Reservation deleted successfully");
            }
            return NotFound("There is no reservation with provided id");
        }

        [HttpPatch("patch-reservation")]
        public IActionResult PatchReservation(ChangeReservationDTO changeReservationDTO)
        {
            if (changeReservationDTO != null && changeReservationDTO.ReservationItems.Count > 0)
            {
                try
                {
                    if (_reservationService.ChangeReservationItems(changeReservationDTO))
                    {
                        return Ok("Items updated successfully");
                    }
                    else
                    {
                        return BadRequest("Token or reservation id is invalid");
                    }
                }
                catch(NonNegativeCapacityException ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return BadRequest("Invalid input data");
            }
        }
    }
}
