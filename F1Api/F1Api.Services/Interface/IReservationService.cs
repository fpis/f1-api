﻿using F1Api.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Interface
{
    public interface IReservationService
    {
        public ReservationResponseDTO AddReservation(ReservationRequestDTO reservation);
        public string? VerifyPromoCode(string promoCode);
        public GetReservationDTO GetReservation(string token, string email);
        public bool Delete(int id);
        public bool ChangeReservationItems(ChangeReservationDTO data);
    }
}
