﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Interface
{
    public interface IDriverService
    {
        public Driver? GetById(int id);
        public ICollection<Driver> GetDriversByGrandPrix(int page, int size, int grandPrixId);
    }
}
