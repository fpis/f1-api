﻿using F1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Interface
{
    public interface IGrandPrixService
    {
        public GrandPrix? GetByName(string name);
    }
}
