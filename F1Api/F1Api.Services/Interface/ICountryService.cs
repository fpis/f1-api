﻿using F1Api.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Interface
{
    public interface ICountryService
    {
        public List<CountryDTO> GetCountries();
    }
}
