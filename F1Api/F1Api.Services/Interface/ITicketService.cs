﻿using F1Api.DataTransferObject;
using F1Api.Domain;
using F1Api.DataTransferObject.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Interface
{
    public interface ITicketService
    {
        public ICollection<TicketDTO> GetTicketByFilters(TicketFilters filters);
    }
}
