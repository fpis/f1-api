﻿using BCrypt.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services
{
    public class ServiceHelper
    {
        private static ServiceHelper instance;
        public static ServiceHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServiceHelper();
                }
                return instance;
            }
        }

        private const string tokenChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private const string promoCodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXY";
        private static readonly Random random = new Random();

        private ServiceHelper() { }

        internal string GenerateToken(int length)
        {
            var tokenBuilder = new StringBuilder();
            for(int i = 0; i < length; i++)
            {
                tokenBuilder.Append(tokenChars[random.Next(tokenChars.Length)]);
            }
            return tokenBuilder.ToString();
        }

        internal string GeneratePromoCode(int length)
        {
            var promoCodeBuilder = new StringBuilder();
            for(int i = 0; i < length; i++)
            {
                promoCodeBuilder.Append(promoCodeChars[random.Next(promoCodeChars.Length)]);
            }
            return promoCodeBuilder.ToString();
        }

        internal string GetHash(string input)
        {
            return BCrypt.Net.BCrypt.EnhancedHashPassword(input, 13);
        }

        public bool VerifyHash(string input, string hash)
        {
            return BCrypt.Net.BCrypt.EnhancedVerify(input, hash);
        }

        public decimal CalculateDaysDiscount(int numberOfDays, decimal totalPrice)
        {
            decimal discountedPrice = 0;
            if (numberOfDays == 2)
            {
                discountedPrice = 0.1m * totalPrice;
            }
            else if (numberOfDays == 3)
            {
                discountedPrice = 0.2m * totalPrice;
            }
            return discountedPrice;
        }
        public decimal CalculateEarlyBirdDiscount(decimal totalPrice)
        {
            return DateTime.Now.CompareTo(new DateTime(2024, 5, 16)) < 0 ? 0.1m * totalPrice : 0;
        }
    }
}
