﻿using AutoMapper;
using F1.Domain;
using F1Api.DataAccessLayer.UOW;
using F1Api.DataTransferObject;
using F1Api.Domain;
using F1Api.Services.Interface;
using F1Api.DataTransferObject.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Implementation
{
    public class TicketService : ITicketService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TicketService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public ICollection<TicketDTO> GetTicketByFilters(TicketFilters filters)
        {
            ICollection<TicketDTO> ticketsDTO = _unitOfWork
                .TicketRepository.GetTicketByFilters(filters)
                .Select(ticket => _mapper.Map<TicketDTO>(ticket))
                .ToList();
            ICollection<TicketDTO> newTickets = new List<TicketDTO>();
            switch(filters.Days.Count) 
            {
                case 1:
                    newTickets = CreateOneDayOffer(ticketsDTO, filters.Days[0]);
                    foreach(var ticket in newTickets)
                    {
                        ticket.EarlyBirdDiscount = ServiceHelper.Instance.CalculateEarlyBirdDiscount(ticket.Price);
                    }
                    break;
                case 2:
                    newTickets = CreateTwoDaysOffer(ticketsDTO.ToList(), filters.Days[0], filters.Days[1]);
                    break;
                case 3:
                    newTickets = CreateThreeDayOffer(ticketsDTO.ToList());
                    break;
            }
            return newTickets.Where(t => t.Price >= filters.LowPrice && t.Price <= filters.HighPrice).ToList();
        }

        private ICollection<TicketDTO> CreateOneDayOffer(ICollection<TicketDTO> tickets, int day)
        {
            DateTime date;
            switch (day)
            {
                case 1:
                    date = new DateTime(2024, 5, 24);
                    break;
                case 2:
                    date = new DateTime(2024, 5, 25);
                    break;
                case 3:
                    date = new DateTime(2024, 5, 26);
                    break;
                default:
                    date = new DateTime(2024, 5, 23);
                    break;
            }
            List<TicketDTO> returnTickets =  tickets.Where((ticket) => ticket.RaceDate != null && ticket.RaceDate?.Date == date.Date).ToList();
            foreach (TicketDTO ticket in returnTickets)
            {
                ticket.RaceIds = new List<int>();
                ticket.RaceIds.Add(ticket.RaceId);
            }
            return returnTickets;
        }

        private ICollection<TicketDTO> CreateTwoDaysOffer(List<TicketDTO> tickets, int day1, int day2)
        {
            ICollection<TicketDTO> offers = new List<TicketDTO>();
            DateTime date1;
            DateTime date2;
            switch (day1)
            {
                case 1:
                    date1 = new DateTime(2024, 5, 24);
                    break;
                case 2:
                    date1 = new DateTime(2024, 5, 25);
                    break;
                case 3:
                    date1 = new DateTime(2024, 5, 26);
                    break;
                default:
                    date1 = new DateTime(2024, 5, 23);
                    break;
            }
            switch (day2)
            {
                case 1:
                    date2 = new DateTime(2024, 5, 24);
                    break;
                case 2:
                    date2 = new DateTime(2024, 5, 25);
                    break;
                case 3:
                    date2 = new DateTime(2024, 5, 26);
                    break;
                default:
                    date2 = new DateTime(2024, 5, 23);
                    break;
            }
            for (int i = 0; i < tickets.Count; i++)
            {
                if (tickets[i].RaceDate != null && tickets[i].RaceDate?.Date == date1.Date)
                {
                    for(int j = i + 1; j < tickets.Count; j++)
                    {
                        if (tickets[j].ZoneName == tickets[i].ZoneName && tickets[j].RaceDate != null && tickets[j].RaceDate?.Date == date2.Date)
                        {
                            decimal totalPrice = tickets[i].Price + tickets[j].Price;
                            TicketDTO newOffer = new TicketDTO()
                            {
                                CurrentCapacity = Math.Min(tickets[i].CurrentCapacity, tickets[j].CurrentCapacity),
                                ZoneName = tickets[i].ZoneName,
                                Price = totalPrice,
                                DaysDiscount = ServiceHelper.Instance.CalculateDaysDiscount(2, totalPrice),
                                EarlyBirdDiscount = ServiceHelper.Instance.CalculateEarlyBirdDiscount(totalPrice),
                                Days = $"{date1.DayOfWeek}, {date2.DayOfWeek}",
                                RaceDate = null,
                                ZoneId = tickets[i].ZoneId,
                            };
                            newOffer.RaceIds = new List<int>();
                            newOffer.RaceIds.Add(tickets[i].RaceId);
                            newOffer.RaceIds.Add(tickets[j].RaceId);
                            offers.Add(newOffer);
                        }
                    }
                }
            }
            return offers;
        }

        private ICollection<TicketDTO> CreateThreeDayOffer(List<TicketDTO> tickets)
        {
            ICollection<TicketDTO> offers = new List<TicketDTO>();
            DateTime date1 = new DateTime(2024, 5, 24);
            DateTime date2 = new DateTime(2024, 5, 25);
            DateTime date3 = new DateTime(2024, 5, 26);
            for (int i = 0; i < tickets.Count; i++)
            {
                if (tickets[i].RaceDate != null && tickets[i].RaceDate?.Date == date1.Date)
                {
                    for (int j = i + 1; j < tickets.Count; j++)
                    {
                        if (tickets[j].ZoneName == tickets[i].ZoneName && tickets[j].RaceDate != null && tickets[j].RaceDate?.Date == date2.Date)
                        {
                            for(int k = j + 1; k < tickets.Count; k++)
                            {
                                if (tickets[k].ZoneName == tickets[j].ZoneName && tickets[k].RaceDate != null && tickets[k].RaceDate?.Date == date3.Date)
                                {
                                    decimal totalPrice = tickets[i].Price + tickets[j].Price + tickets[k].Price;
                                    TicketDTO newOffer = new TicketDTO()
                                    {
                                        CurrentCapacity = Math.Min(tickets[i].CurrentCapacity, Math.Min(tickets[j].CurrentCapacity, tickets[k].CurrentCapacity)),
                                        ZoneName = tickets[i].ZoneName,
                                        Price = totalPrice,
                                        DaysDiscount = ServiceHelper.Instance.CalculateDaysDiscount(3, totalPrice),
                                        EarlyBirdDiscount = ServiceHelper.Instance.CalculateEarlyBirdDiscount(totalPrice),
                                        Days = $"{date1.DayOfWeek}, {date2.DayOfWeek}, {date3.DayOfWeek}",
                                        ZoneId = tickets[i].ZoneId,
                                    };
                                    newOffer.RaceIds = new List<int>();
                                    newOffer.RaceIds.Add(tickets[i].RaceId);
                                    newOffer.RaceIds.Add(tickets[j].RaceId);
                                    newOffer.RaceIds.Add(tickets[k].RaceId);
                                    offers.Add(newOffer);
                                }
                            }
                        }
                    }
                }
            }
            return offers;
        }
        
    }
}
