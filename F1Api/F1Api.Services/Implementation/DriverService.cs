﻿using F1.Domain;
using F1Api.DataAccessLayer.UOW;
using F1Api.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Implementation
{
    public class DriverService : IDriverService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DriverService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Driver? GetById(int id)
        {
            return _unitOfWork.DriverRepository.GetById(id);
        }

        public ICollection<Driver> GetDriversByGrandPrix(int page, int size, int grandPrixId)
        {
            return _unitOfWork.DriverRepository.GetDriversByGrandPrix(page, size, grandPrixId).ToList();
        }
    }
}
