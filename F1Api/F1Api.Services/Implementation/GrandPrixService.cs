﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using F1.Domain;
using F1Api.DataAccessLayer.UOW;
using F1Api.Services.Interface;

namespace F1Api.Services.Implementation
{
    public class GrandPrixService : IGrandPrixService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GrandPrixService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public GrandPrix? GetByName(string name)
        {
            return _unitOfWork.GrandPrixRepository.GetByName(name);
        }
    }
}
