﻿using AutoMapper;
using F1.Domain;
using F1Api.DataAccessLayer.UOW;
using F1Api.DataTransferObject;
using F1Api.Domain;
using F1Api.Services.Exceptions.Reservation;
using F1Api.Services.Interface;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Implementation
{
    public class ReservationService : IReservationService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ReservationService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public ReservationResponseDTO AddReservation(ReservationRequestDTO reservation)
        {
            Reservation r = _mapper.Map<Reservation>(reservation);
            
            City? c = _unitOfWork.CityRepository.GetById(reservation.PostalCode);
            if (c != null)
            {
                r.Customer.CityId = c.ZipCode;
                r.Customer.City = null;
            }

            bool promoCodeUnique = false;
            string returnPromoCode = string.Empty;
            while(!promoCodeUnique)
            {
                string generatedPromoCode = ServiceHelper.Instance.GeneratePromoCode(10);
                string hashedPromoCode = ServiceHelper.Instance.GetHash(generatedPromoCode);
                string? promoCode = _unitOfWork.ReservationRepository.GetPromoCode(hashedPromoCode);
                if (promoCode == null)
                {
                    promoCodeUnique = true;
                    r.PromoCode = hashedPromoCode;
                    returnPromoCode = generatedPromoCode;
                }
            }

            bool tokenUnique = false;
            string returnToken = string.Empty;
            while(!tokenUnique)
            {
                string generatedToken = ServiceHelper.Instance.GenerateToken(24);
                string hasedToken = ServiceHelper.Instance.GetHash(generatedToken);
                string? token = _unitOfWork.ReservationRepository.GetToken(hasedToken);
                if(token == null)
                {
                    tokenUnique = true;
                    r.Token = hasedToken;
                    returnToken = generatedToken;
                }
            }

            List<ReservationItemsDTO> reservationItemHelpers = new List<ReservationItemsDTO>();
            foreach(var reservationItem in reservation.ReservationItems)
            {
                foreach(var raceId in reservationItem.RaceIds)
                {
                    reservationItemHelpers.Add(new ReservationItemsDTO()
                    {
                        RaceId = raceId,
                        ZoneId = reservationItem.ZoneId
                    });
                }
            }
            List<Ticket> tickets = _unitOfWork.TicketRepository.GetTicketsById(reservationItemHelpers).ToList();
            foreach(var reservationItem in reservation.ReservationItems)
            {
                foreach(var raceId in reservationItem.RaceIds)
                {
                    foreach (var ticket in tickets)
                    {
                        if (ticket.ZoneId == reservationItem.ZoneId && ticket.RaceId == raceId)
                        {
                            reservationItem.Price += ticket.Price;
                            ticket.CurrentCapacity -= reservationItem.Quantity;
                        }
                    }
                }  
            }

            r.TotalPrice = reservation.ReservationItems.Sum(x => x.Price * x.Quantity);
            r.DaysDiscount = reservation.ReservationItems.Sum(ri => ServiceHelper.Instance.CalculateDaysDiscount(ri.RaceIds.Count, ri.Price * ri.Quantity));
            r.EarlyBirdDiscount = reservation.ReservationItems.Sum(ri => ServiceHelper.Instance.CalculateEarlyBirdDiscount(ri.Price * ri.Quantity));
            if (reservation.PromoCode != null && _unitOfWork.ReservationRepository.VerifyPromoCode(reservation.PromoCode) != null)
            {
                _unitOfWork.ReservationRepository.DeactivatePromoCode(reservation.PromoCode);
                r.PromoCodeDiscount = 0.05m * r.TotalPrice;
            }
            r.DiscountedPrice = r.TotalPrice - r.DaysDiscount - r.EarlyBirdDiscount - r.PromoCodeDiscount;

            r.ReservationDate = DateTime.Now;

            ReservationResponseDTO response = _mapper.Map<ReservationResponseDTO>(_unitOfWork.ReservationRepository.SaveReservation(r));

            try
            {
                _unitOfWork.SaveChangesWithIdentityInsertOn<City>();
            }
            catch(Exception ex)
            {
                if (ex.InnerException.Message.ToLower().Contains("ck_current_capacity_non_negative"))
                {
                    throw new NonNegativeCapacityException("Some items have quantity greater then capacity");
                }
            }
            response.Token = returnToken;
            response.PromoCode = returnPromoCode;

            return response;
        }

        public string? VerifyPromoCode(string promoCode)
        {
            return _unitOfWork.ReservationRepository.VerifyPromoCode(promoCode);
        }

        public GetReservationDTO GetReservation(string token, string email)
        {
            Reservation r = _unitOfWork.ReservationRepository.GetReservationByEmailAndToken(email, token);
            if (r == null) return null;
            GetReservationDTO returnReservation = _mapper.Map<GetReservationDTO>(r);

            int serialNumber = r.ReservationItems[0].SerialNumber;
            int dayInItem = r.ReservationItems[0].DayInItem;
            returnReservation.ReservationItems.Add(new GetReservationItemsDTO()
            {
                MaximumQuantity = int.MaxValue,
                Days = ""
            });
            for (int i = 0; i < r.ReservationItems.Count; i++)
            {
                returnReservation.ReservationItems[serialNumber - 1].Quantity = r.ReservationItems[i].Quantity;
                returnReservation.ReservationItems[serialNumber - 1].ZoneName = r.ReservationItems[i].Ticket.Zone.Name;
                returnReservation.ReservationItems[serialNumber - 1].Price += r.ReservationItems[i].Ticket.Price;
                returnReservation.ReservationItems[serialNumber - 1].FullPrice += r.ReservationItems[i].Ticket.Price;
                returnReservation.ReservationItems[serialNumber - 1].MaximumQuantity = Math.Min(returnReservation.ReservationItems[serialNumber - 1].MaximumQuantity, _unitOfWork.TicketRepository.GetMaximumQuantity(r.ReservationItems[i].RaceId, r.ReservationItems[i].ZoneId));
                returnReservation.ReservationItems[serialNumber - 1].Days += returnReservation.ReservationItems[serialNumber - 1].Days.Length == 0 ?
                    _unitOfWork.RaceRepository.GetDayInWeekNameById(r.ReservationItems[i].RaceId) : $", {_unitOfWork.RaceRepository.GetDayInWeekNameById(r.ReservationItems[i].RaceId)}";
                returnReservation.ReservationItems[serialNumber - 1].NumberOfDays++;
                returnReservation.ReservationItems[serialNumber - 1].SerialNumber = serialNumber;
                dayInItem--;
                if (dayInItem == 0 && i != r.ReservationItems.Count - 1)
                {
                    serialNumber++;
                    returnReservation.ReservationItems.Add(new GetReservationItemsDTO()
                    {
                        Days = "",
                        MaximumQuantity = int.MaxValue
                    });
                    dayInItem = r.ReservationItems[i + 1].DayInItem;
                }
            }
            foreach (var item in returnReservation.ReservationItems) 
            {
                item.Price -= ServiceHelper.Instance.CalculateEarlyBirdDiscount(item.Price);
                item.Price -= ServiceHelper.Instance.CalculateDaysDiscount(item.NumberOfDays, item.Price);
            }

            return returnReservation;
        }

        public bool Delete(int id)
        {
            bool success = _unitOfWork.ReservationRepository.Delete(id);
            _unitOfWork.SaveChanges();
            return success;
        }

        public bool ChangeReservationItems(ChangeReservationDTO data)
        {
            try
            {
                bool success = _unitOfWork.ReservationRepository.UpdateReservationItems(data);
                _unitOfWork.SaveChanges();
                return success;
            }catch (Exception ex)
            {
                throw new NonNegativeCapacityException("Some items have quantity greater then capacity");
            }
        }
    }
}
