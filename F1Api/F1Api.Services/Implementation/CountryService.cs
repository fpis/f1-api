﻿using AutoMapper;
using F1Api.DataAccessLayer.UOW;
using F1Api.DataTransferObject;
using F1Api.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Implementation
{
    public class CountryService : ICountryService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CountryService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public List<CountryDTO> GetCountries()
        {
            return _unitOfWork.CountryRepository.GetAll().Select(c => _mapper.Map<CountryDTO>(c)).ToList();
        }
    }
}
