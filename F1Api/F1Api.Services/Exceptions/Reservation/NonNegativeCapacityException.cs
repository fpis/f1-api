﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1Api.Services.Exceptions.Reservation
{
    public class NonNegativeCapacityException : Exception
    {
        public NonNegativeCapacityException(string message) : base(message) { }
    }
}
